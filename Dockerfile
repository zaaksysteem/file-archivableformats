FROM perl:latest

WORKDIR /tmp/build
COPY dev-bin/cpanm .
COPY cpanfile .
RUN ./cpanm --installdeps .

# Run without shared-mime-info (CPAN testers failure)
RUN apt-get purge -y shared-mime-info

COPY . .

RUN prove -l && ./cpanm .

RUN apt-get update \
  && apt-get install --no-install-recommends -y shared-mime-info \
  && apt-get clean && apt-get autoremove -y \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/* ~/.cpanm

RUN prove -l && ./cpanm .
